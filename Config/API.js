class API {
  constructor(){
    return {
      baseURI: 'http://router.project-osrm.org',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      mapMatchingEndPoint: '/match/v1/driving/',
    };
  }
}

export default API;