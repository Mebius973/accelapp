import React from 'react';
import { StyleSheet, View, Platform } from 'react-native';
import HockeyApp from 'react-native-hockeyapp';
import MainPage from './Components/MainPage';
import Countdown from './Components/Countdown';
import Score from './Components/Score';
import StartButton from './Components/StartButton';

class App extends React.Component {
  componentWillMount() {
    var HOCKEYAPP_APP_ID = "350b5443a43d4053a809ff1888c0eb52";
    HockeyApp.configure(HOCKEYAPP_APP_ID, true);
  }

  async componentDidMount() {
    HockeyApp.start();
    HockeyApp.checkForUpdate();
  }

  render() {
    return (
      <View style={styles.container}>
        <MainPage />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default App;