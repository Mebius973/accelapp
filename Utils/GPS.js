import { Platform, PermissionsAndroid } from 'react-native';
import API from '../Config/API';
import Frisbee from 'frisbee';
import React from 'react';

class GPS {
  constructor(api){
    switch (Platform.OS){
      case 'ios':
        navigator.geolocation.requestAuthorization();
        break;
      case 'android':
        requestLocationPermission();
        break;
      default:
        console.log('Unknow OS, no request to use GPS issued');
    }
    this.locations = [];
    this.matchedLocations = [];
    this.API = api;
    this.apiClient = new Frisbee({
      baseURI: this.API.baseURI,
      headers: this.API.headers
    });
  }

  _locationToString = () => {
    let result = "";
    if(this.locations.length > 1){
      for (var i = 0; i < this.locations.length - 2; i++) {
        result = result + this.locations[i].longitude + "," + this.locations[i].latitude + ";";
      }
      result = result + this.locations[this.locations.length - 1].longitude + "," + this.locations[this.locations.length - 1].latitude;
    }
    return result;
  }

  _precisionToString = () => {
    let result = "radiuses=";
    if(this.locations.length > 1){
      for (var i = 0; i < this.locations.length - 2; i++) {
        result = result + this.locations[i].accuracy + ";";
      }
      result = result + this.locations[this.locations.length - 1].accuracy;
    }
    return result;
  }

  _timestampToString = () => {
    let result = "timestamps=";
    if(this.locations.length > 1){
      for (var i = 0; i < this.locations.length - 2; i++) {
        result = result + this.locations[i].timestamp + ";";
      }
      result = result + this.locations[this.locations.length - 1].timestamp;
    }
    return result;
  }

  _updateLocations = (position) => {
    if (this.locations.length > 5){
      this.locations.shift()
    }
    this.locations = this.locations.concat({
      longitude: position.coords.longitude,
      latitude: position.coords.latitude,
      accuracy: position.coords.accuracy,
      timestamp: Date.now()
    });
  }

  _updateMatchedLocations(tracepoints){
    for(var i = 0; i < tracepoints.length - 1; i++){
      if(tracepoints[i]){
        this.matchedLocations = this.matchedLocations.concat({
          longitude: tracepoints[i].location[0],
          latitude: tracepoints[i].location[1],
          timestamp: this.locations[i].timestamp
        });
      }
    }
  }

  _calculateDistance = (lastTracePoint, currentTracePoint) => {
    var radius = 6371 * 1000;
    var toRad = function(number) {
      return number * Math.PI / 180;
    };

    var lonDistance = toRad(currentTracePoint.longitude - lastTracePoint.longitude);
    var latDistance = toRad(currentTracePoint.latitude - lastTracePoint.latitude);
    var a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2) +
    Math.cos(toRad(lastTracePoint.latitude)) * Math.cos(toRad(currentTracePoint.latitude)) *
    Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
    return radius * (2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a)));
  }

  _calculateSpeed = () => {
    let result;
    if(this.matchedLocations.length > 1){
      const lastTracePoint = this.matchedLocations[this.matchedLocations.length - 2];
      const currentTracePoint = this.matchedLocations[this.matchedLocations.length - 1];
      const distance = this._calculateDistance(lastTracePoint, currentTracePoint);
      const elapsedTime = currentTracePoint.timestamp - lastTracePoint.timestamp;
      result = distance / (elapsedTime / 1000);
    }
    else{
      result = 0;
    }
    return result;
  }

  getDirectLocation(){
    return this.locations.length > 0 ? this.locations[this.locations.length - 1].latitude.toString().substring(0,7) + " ; " + this.locations[this.locations.length - 1].latitude.toString().substring(0,7) : "";
  }

  getAccuracy(){
    return this.locations.length > 0 ? this.locations[this.locations.length - 1].accuracy : "";
  }

  getMatchedLocation(){
    return this.matchedLocations.length > 0 ? this.matchedLocations[this.matchedLocations.length - 1].latitude.toString().substring(0,7) + " ; " + this.matchedLocations[this.matchedLocations.length - 1].latitude.toString().substring(0,7) : "";
  }

  init(onErrorCallback=null){
    if(this.locations.length < 2){
      navigator.geolocation.getCurrentPosition(
        (position) => {
          if(position.coords.accuracy >= 0 && position.coords.accuracy <= 50){
            if(this.locations.length >0 &&
              this.locations[0].longitude == position.coords.longitude &&
              this.locations[0].latitude == position.coords.latitude){
              this._updateLocations(position);
            }
            else{
              this._updateLocations(position);
            }
          }
          this.init(onErrorCallback);
        },
        (error) => onErrorCallback(error),
        { enableHighAccuracy: true}
      )
    }
  }

  start(onUpdateCallback, onErrorCallback=null, spamGPSUpdate=false){
    this.spamGPSUpdate = spamGPSUpdate;
    if(this.spamGPSUpdate){
      this.mainIntervalId = setInterval(() => {
        navigator.geolocation.getCurrentPosition(
          (position) => {
            console.log("Direct speed: " + position.coords.speed);
            console.log("Direct accuracy: " + position.coords.accuracy);
            if(position.coords.accuracy >= 0 && position.coords.accuracy <= 50){
              this._updateLocations(position);
              const currentLocations = this._locationToString();
              const associatedPrecision = this._precisionToString();
              const associatedTimestamps = this._timestampToString();
              console.log("Request URL: " + this.API.mapMatchingEndPoint + currentLocations + "?" + associatedPrecision + "&" + associatedTimestamps);
              this.apiClient.get(this.API.mapMatchingEndPoint + currentLocations + "?" + associatedPrecision + "&" + associatedTimestamps)
              .then((response) => {
                console.log(response.body);
                if(response.body.code == "Ok" && response.body.tracepoints.length > 1){
                  this._updateMatchedLocations(response.body.tracepoints);
                  var speed = this._calculateSpeed();
                  onUpdateCallback(speed);
                }
              })
              .catch(function (error) {
                handlePromiseError(error);
              });
            }
          },
          (error) => onErrorCallback(error),
          { enableHighAccuracy: true}
        )
      }, 10);

      if(Platform.OS == 'android'){
        this.watchId = navigator.geolocation.watchPosition(
          (position) => {
          },
          (error) => console.log(error.message),
          { enableHighAccuracy: true, maximumAge: 0, distanceFilter: 1 }
        )
      }
    }
    else{
      this.watchId = navigator.geolocation.watchPosition(
        (position) => {
          console.log("Direct speed: " + position.coords.speed);
          console.log("Direct accuracy: " + position.coords.accuracy);
          if(position.coords.accuracy >= 0 && position.coords.accuracy <= 50){
            this._updateLocations(position);
            const associatedPrecision = this._precisionToString();
            const currentLocations = this._locationToString();
            const associatedTimestamps = this._timestampToString();
            console.log("Request URL: " + this.API.mapMatchingEndPoint + currentLocations + "?" + associatedPrecision + "&" + associatedTimestamps);
            this.apiClient.get(this.API.mapMatchingEndPoint + currentLocations + "?" + associatedPrecision + "&" + associatedTimestamps)
            .then((response) => {
              console.log(response.body);
              if(response.status == 200 && response.body.code == "Ok" && response.body.tracepoints.length > 1){
                this._updateMatchedLocations(response.body.tracepoints);
                var speed = this._calculateSpeed();
                onUpdateCallback(speed);
              }
            })
            .catch(function(error) {
              handlePromiseError(error);
            });
          }
        },
        (error) => onErrorCallback(error),
        { enableHighAccuracy: true, distanceFilter: 1 }
      )
    }
  }

  stop(){
    if(this.spamGPSUpdate){
      clearInterval(this.mainIntervalId);
      if(Platform.OS == 'android'){
        navigator.geolocation.clearWatch(this.watchId);
      }
    }
    else{
      navigator.geolocation.clearWatch(this.watchId);
    }
  }
}

function handlePromiseError(error) {
  if (error.response) {
    // The request was made and the server responded with a status code
    // that falls out of the range of 2xx
    console.log(error.response.data);
    console.log(error.response.status);
    console.log(error.response.headers);
  } else if (error.request) {
    // The request was made but no response was received
    // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
    // http.ClientRequest in node.js
    console.log(error.request);
  } else {
    // Something happened in setting up the request that triggered an Error
    console.log('Error', error.message);
  }
  console.log(error.config);
}

async function requestLocationPermission() {
  try {
    const granted = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
      {
        'title': 'Location access',
        'message': 'Location access is required to track your speed.'
      }
    )
    if (granted === PermissionsAndroid.RESULTS.GRANTED) {
      console.log("You can use the location")
    } else {
      console.log("Location permission denied")
    }
  } catch (err) {
    console.warn(err)
  }
}

export default GPS;