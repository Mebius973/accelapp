import React from 'react';
import { Text, View, StyleSheet } from 'react-native';
import SocialShare from './SocialShare';

class Score extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.scorePlaceHolder}>You're 0-100 km/h score is:</Text>
        <View style={styles.circle}>
          <Text style={styles.score}>{this.props.timeText}</Text>
        </View>
        <SocialShare style={{justifyContent: 'flex-end'}}/>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-around',
    alignItems: 'center'
  },
  scorePlaceHolder: {
    marginTop: 30,
    fontSize: 16
  },
  circle: {
    marginTop: 20,
    width: 150,
    height: 150,
    borderRadius: 150/2,
    borderColor: 'rgba(0, 122, 255, 1)',
    borderWidth: 1
  },
  score:{
    marginTop: 60,
    marginLeft: 40,
    fontSize: 24,
    color: 'rgba(0, 122, 255, 1)'
  }
})

export default Score;