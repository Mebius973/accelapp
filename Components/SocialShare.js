import React from 'react';
import { StyleSheet, Text, View, TouchableHighlight, Image, Share } from 'react-native';

class SocialShare extends React.Component {
  _share = () => {
    Share.share({
      title: "Share your record",
      message: "I've reached 100km/h in only " + this.props.time + "seconds! Any challenger?!"
    });
  }

  render() {
    return (
      <TouchableHighlight style={styles.radius} onPress={this._share} underlayColor="rgba(0, 122, 255, 0.1)">
        <View style={[styles.container, styles.radius]}>
          <Text style={styles.placeHolder}>Share it !</Text>
          <Image source={require('../Images/Share.png')} style={styles.image} />
        </View>
      </TouchableHighlight>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: 'rgba(0, 122, 255, 1)',
    borderWidth: 1,
    paddingHorizontal: 20,
    paddingVertical: 10
  },
  radius:{
    borderWidth: 1,
    borderRadius: 50
  },
  imageRow: {
    flexDirection: 'row'
  },
  placeHolder: {
    fontSize: 20
  },
  image: {
    width: 30,
    height: 30,
    marginLeft: 10,
    opacity: 0.8
  }
})

export default SocialShare;