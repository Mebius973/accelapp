import React from 'react';
import { StyleSheet, Text, View, TouchableHighlight } from 'react-native';

class StartButton extends React.Component {
  render() {
    if(this.props.onPress){
      return (
        <TouchableHighlight style={styles.circle} onPress={this.props.onPress} underlayColor="white">
          <View style={styles.container}>
            <Text style={styles.innerText}>Start</Text>
          </View>
        </TouchableHighlight>
      );
    }
    else{
      return (
        <TouchableHighlight style={styles.circle} underlayColor="white">
          <View style={styles.container}>
            <Text style={styles.innerText}>Start</Text>
          </View>
        </TouchableHighlight>
      );
    }
  }
}

const styles = StyleSheet.create({
  circle: {
      width: 150,
      height: 150,
      borderRadius: 150/2,
      backgroundColor: 'rgba(0, 122, 255, 1)'
  },

  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center'
  },

  innerText: {
    fontSize: 24,
    color: 'white'
  }
})

export default StartButton;