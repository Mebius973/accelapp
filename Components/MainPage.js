import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import StartButton from './StartButton';
import Speedometer from './Speedometer';
import Score from './Score';
import GPS from '../Utils/GPS';
import API from '../Config/API';

class MainPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
        tapped: false,
        speed: 0,
        startTime: null,
        elapsedTime: null,
        timerId: null,
        error: null
    }
    this.API = new API();
    this.GPS = new GPS(this.API);
    this.spamGPSUpdate = true;
  }

  _startTimer = () => {
    this.setState({startTime: Date.now()});
    var timerId = setInterval(() => {
      this.setState({elapsedTime: (Date.now() - this.state.startTime)});
    }, 10);
    this.setState({
      timerId: timerId
    });
  }

  _resetTimer = () => {
    this.setState({
      startTime: Date.now(),
      elapsedTime: 0
    });
  }

  _stopTimer = () => {
    clearInterval(this.state.timerId);
  }

  _onPressButton = () => {
    this.setState({
      tapped: true
    });
    this._startTimer();
  }

  _onSpeedUpdate = (newSpeed) => {
    console.log("speed: " + newSpeed);
    if(newSpeed > 2){
      this._resetTimer();
    }
    if(newSpeed > 100){
      this._stopTimer();
    }
    this.setState({
      speed: newSpeed * 3.6
    });
  }

  componentDidMount() {
    var startPing = Date.now();
    fetch(this.API.baseURI)
    .then((response) => {
       var stopPing = Date.now();
       console.log("ping: " + (stopPing - startPing));
     })
    .catch((error) => {
         console.log('network error: ' + error);
     })
    this.GPS.init((error) => { this.setState({error: error}); });
  }

  render() {
    const tapped = this.state.tapped;
    const speed = this.state.speed;
    if(!tapped){
      return (
        <View style={styles.container}>
          <StartButton onPress={this._onPressButton} />
        </View>
        );
    }
    else if(speed < 100){
      if(this.state.error){
        return (
          <View style={styles.container}>
            <Speedometer GPS={this.GPS} onSpeedUpdate={this._onSpeedUpdate} speed={speed} spamGPSUpdate={this.spamGPSUpdate} error={this.state.error}/>
          </View>
        );
      }
      else{
       return (
          <View style={styles.container}>
            <Speedometer GPS={this.GPS} onSpeedUpdate={this._onSpeedUpdate} speed={speed} spamGPSUpdate={this.spamGPSUpdate}/>
          </View>
        );
      }
    }
    else{
      this._stopTimer();
      const elapsedTime = msToTime(this.state.elapsedTime);
      return (
        <View style={styles.container}>
          <Score timeText={elapsedTime} />
        </View>
      );
    }
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center'
  }
})

function msToTime(duration) {
  var milliseconds = parseInt((duration%1000)/10)
      , seconds = parseInt(duration/1000);

  return seconds + "." + milliseconds + " s";
}

export default MainPage;