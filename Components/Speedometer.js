import React from 'react';
import { StyleSheet, Text, View, Platform } from 'react-native';
import Circle from './Circle';

class Speedometer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
        error: null
    }
  }

  componentDidMount() {
    this.props.GPS.start(this.props.onSpeedUpdate, (error) => { this.setState({error: error}); }, this.props.spamGPSUpdate);
  }

  componentWillUnmount() {
    this.props.GPS.stop();
  }

  render() {
    if(this.props.error){
      console.log(this.props.error);
    }
    const directLocation = this.props.GPS.getDirectLocation();
    const accuracy = this.props.GPS.getAccuracy();
    const matchedLocation = this.props.GPS.getMatchedLocation();
    return (
      <View style={styles.container}>
        {this.state.error ? <Text>Error: {this.state.error}</Text> : null}
        <Text>Direct Location: {directLocation}</Text>
        <Text>Accuracy: {accuracy}</Text>
        <Text>Matched Location: {matchedLocation}</Text>
        <Circle
          size={150}
          progress={Math.floor(this.props.speed) / 100}
          animated={false}
          showsText={true}
          formatText={progress => `${Math.round(progress * 100)} km/h`}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center'
  }
})

export default Speedometer;