import React from 'react';
import { StyleSheet, Text, View, Animated, Easing } from 'react-native';

class AnimatedText extends React.Component {
  state = {
    animatedValue: new Animated.Value(0)
  }

  componentDidMount() {
    this.animate()
  }

  animate(){
    this.state.animatedValue.setValue(0);
    Animated.timing(this.state.animatedValue, {
      toValue: 1,
      easing: Easing.linear(),
      duration: 2000,
    }).start(() => this.animate());
  }

  render(){
    const scaleText = this.state.animatedValue.interpolate({
      inputRange: [0, 0.5, 1],
      outputRange: [1, 3, 1]
    })

    return (
      <Animated.Text style={{transform: [{scale: scaleText}]}}>{this.props.text}</Animated.Text>
    )
  }
}

export default AnimatedText;