import React from 'react';
import AnimatedText from './AnimatedText';
import AnimatedNumber from './AnimatedNumber';

class Countdown extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      step: 0,
      steps: [
        { text: "Ready ?", duration: 2 },
        { text: "3", duration: 1 },
        { text: "2", duration: 1 },
        { text: "1", duration: 1 },
        { text: "Go !", duration: 2 },
      ]
    };
  }

  // componentDidMount() {
    // var intervalId = setInterval(() => {
    //   const step = this.state.step + 1;
    //   if(step > (this.state.steps.length - 1)){
    //     clearInterval(intervalId);
    //     return ;
    //   }
    //   this.setState({
    //     step: step
    //   });
    // }, 2 * this.state.duration * 1000 );
  // }

  render() {
    const step = this.state.step;
    const text = this.state.steps[step].text;
    if(step == 0 || step == 4){
      return (
        <AnimatedText text={text} />
      );
    }
    else{
      return (
        <AnimatedNumber text={text} />
      );
    }
  }
}

export default Countdown;