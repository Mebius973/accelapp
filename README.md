#AccelApp
The goal of this repo is to build a multi-platform (ios and Android) App that aims at measuring 0-100 km/h acceleration on vehicules using React Native.

#MVP
A single page app with a 0-100 km/h cursor that fills up to 100 km/h while on movement. It then freezes full and displays the time it require to reach this speed.

It should start the timer once the detected speed is > 0 km/h. This could be modulate with an acceleration threshold to avoid detecting not vehicule related movements (putting your phone in your pocket...)
